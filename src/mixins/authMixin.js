export default {
    methods: {
        isLoggedIn() {
            var isUserLoggedIn = false;
            var token = this.getToken();
            if (token.payload) {
                isUserLoggedIn = new Date(token.payload.exp * 1000) > new Date();
            }

            return isUserLoggedIn;
        },

        getToken() {
            var token = {}
            token.raw = localStorage.getItem("token");
            if (token.raw) {
                token.header = JSON.parse(window.atob(token.raw.split('.')[0]));
                token.payload = JSON.parse(window.atob(token.raw.split('.')[1]));
            }

            return token;

        }
    }
}